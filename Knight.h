#pragma once

#include <string>
#include <vector>
#include "Piece.h"
#include "Type.h"

class Knight : public Piece
{
public:
	Knight(int team, Point& point);
	//virtual ~Knight();

	virtual void eat(Piece* other) override;
	virtual void move(const Point& dest, std::vector<Piece*>& pieces) override;

};
