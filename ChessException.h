#pragma once

#include <exception>

/*
* exception for a case where a player made a chess move
* one virtual function, returns the exception reason
*/

class ChessException : public std::exception
{
public:
	virtual const char* what() const
	{
		return "Chess happend and the game is over!";
	}

};