#pragma once

#include <exception>

/*
* exception for a case where a used point isn't valid
* one virtual function, returns the exception reason
*/

class InvalidPoint : public std::exception
{
public:
	virtual const char* what() const
	{
		return "This point is not valid!";
	}

};