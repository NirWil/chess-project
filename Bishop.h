#pragma once

#include <string>
#include <vector>
#include <iostream>
#include "Piece.h"
#include "Type.h"

class Bishop : public Piece
{
public:
	Bishop(int team, Point& point);
	virtual void eat(Piece* other) override;
	virtual void move(const Point& dest, std::vector<Piece*>& pieces) override;

};

