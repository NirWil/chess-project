#include "Rook.h"

/*
* the constructor
* set the team, point and type
* type in this case, always going to be ROOK
*/
Rook::Rook(int team, Point& point)
	:Piece(team, TYPE::ROOK, point)
{

}

/*
* the eat function
* taking care of case when a rook piece eating another piece
* gets pointer to the other piece (that being eaten)
* returns nothing
*/
void Rook::eat(Piece* other)
{
	other->setDead(true);
	this->getPoint() = other->getPoint();
	std::cout << this->getPieceAsChar() << " Is eating " << other->getPieceAsChar() << std::endl;
}


/*
* the move function
* taking care of case when a rook move from one place to another
* gets the dest point, and all of the other pieces in the board
* returns nothing
*/
void Rook::move(const Point& dest, std::vector<Piece*>& pieces)
{
	Point point(1, 1);
	bool result = true;
	if (abs(this->getPoint().getX() - dest.getX()) > 0 //if x distance is greater than 0 and y is 0
		&& abs(this->getPoint().getY() - dest.getY()) == 0)
	{
		point.setY(dest.getY());
		if (this->getPoint().getX() < dest.getX()) //if moving right
		{
			for (int x = this->getPoint().getX() + 1; x < dest.getX(); x++)
				//if he is passing over something
			{
				point.setX(x);
				if (getPieceAtPoint(point, pieces) != nullptr) //if he hits a piece
				{
					result = false;
					break;
				}
			}
		}
		else if (this->getPoint().getX() > dest.getX()) //if moving left
		{
			for (int x = this->getPoint().getX() - 1; x > dest.getX(); x--)
				//if he is passing over something
			{
				point.setX(x);
				if (getPieceAtPoint(point, pieces) != nullptr) //if he hits a piece
				{
					result = false;
				}
			}
		}
	}
	else if (abs(this->getPoint().getX() - dest.getX()) == 0 //if x distance is greater than 0 and y is 0
		&& abs(this->getPoint().getY() - dest.getY()) > 0)
	{
		point.setX(dest.getX());
		if (this->getPoint().getY() < dest.getY()) //if moving up
		{
			for (int y = this->getPoint().getY() + 1; y < dest.getY(); y++)
				//if he is passing over something
			{
				point.setY(y);
				if (getPieceAtPoint(point, pieces) != nullptr) //if he hits a piece
				{
					result = false;
					break;
				}
			}
		}
		else if (this->getPoint().getY() > dest.getY()) //if moving down
		{
			for (int y = this->getPoint().getY() - 1; y > dest.getY(); y--)
				//if he is passing over something
			{
				point.setY(y);
				if (getPieceAtPoint(point, pieces) != nullptr) //if he hits a piece
				{
					result = false;
				}
			}
		}
	}
	else
	{
		result = false;
	}

	doesEat(result, dest, getPieceAtPoint(dest, pieces));
}

