#pragma once

#include <iostream>
#include <string>
#include <vector>
#include "Piece.h"
#include "Type.h"

class Pawn : public Piece
{
public:
	Pawn(int team, Point& point);

	virtual void eat(Piece* other) override;
	virtual void move(const Point& dest, std::vector<Piece*>& pieces) override;
	bool canPawnEat(const Point& dest, std::vector<Piece*>& pieces);
	bool firstTimeMove(const Point& dest, std::vector<Piece*>& pieces);
	bool regularMove(const Point& dest, std::vector<Piece*>& pieces);

private:
	bool isFirstMove;
};
