#include "Point.h"

/*
* CTOR for creating a Point instance.
*/
Point::Point(const unsigned int x, const unsigned int y)
{
	this->_x = Point::intChecker(x);
	this->_y = Point::intChecker(y);

	Point::vallidChecker(this->_x);
	Point::vallidChecker(this->_y);
}

Point::Point()
{
	this->_x = 0;
	this->_y = 0;
}

/*
* A static function that throws exception if the point given to it is not vallid
* input: int of a point
* output: none - throws exception if the point is ilegall
*/
void Point::vallidChecker(int point)
{
	if (point < MIN_POINT || point > MAX_POINT)
	{
		throw InvalidPoint();
	}
}


/*
* A static function that makes sure that the point recived is always int - 
* even if a char was recived or int with a value of a assci char
* input: int of a point
* output: the point as a char
*/
int Point::intChecker(int point)
{
	if (point >= 'a') //if y is given as a char of letter
	{
		point -= 'a';
		point += 1; //cant be 0
	}
	else if (point >= '0') //if y is given as a char of number
	{
		point -= '0';
	}
	return point;
}


/*
* A method that returns the value of Y
* input: none
* output: the point
*/
int Point::getX() const
{
	return this->_x;
}


/*
* A method that returns the value of Y
* input: none
* output: the point
*/
int Point::getY() const
{
	return this->_y;
}


/*
* A method that gets Y as a char and not int
* input: none
* output: a char of Y
*/
char Point::getXasChar() const
{
	return this->_x + 'a'; //x is usually counted as a char from a to h
}

/*
* A method that gets Y as a char and not int
* input: none
* output: a char of Y
*/
char Point::getYasChar() const
{
	return this->_y + '0'; //y is usually counted as a char from 1 and 8
}


/*
* A function that gets a value, checks its an integar, if not converts it,
* and then checks if its vallid -if not - it throws an exception, if it is changes the X value
* input: int x
* output: none
*/
void Point::setX(int x)
{
	x = Point::intChecker(x);
	Point::vallidChecker(x);
	this->_x = x;
}

/*
* A function that gets a value, checks its an integar, if not converts it,
* and then checks if its vallid -if not - it throws an exception, if it is changes the Y value
* input: int y
* output: none
*/
void Point::setY(int y)
{
	y = Point::intChecker(y);
	Point::vallidChecker(y);
	this->_y = y;
}

/*
* input: the left size of the == operator, and the right side - two points
* output: a bollean result if they are equal in their x and their y
* An opoerator that compare beetwen two points
*/
bool operator==(Point lhs, Point rhs)
{
	bool result = false;
	if (lhs.getX() == rhs.getX() && lhs.getY() == rhs.getY())
	{
		result = true;
	}
	return result;
}
