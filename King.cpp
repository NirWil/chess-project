#include "King.h"

/*
* the constructor
* set the team, point and type
* type in this case, always going to be KING
*/
King::King(int team, Point& point)
	:Piece(team, TYPE::KING, point)
{
}

/*
* the eat function
* taking care of case when a king piece eating another piece
* gets pointer to the other piece (that being eaten)
* returns nothing
*/
void King::eat(Piece* other)
{
	//before eating, check if you are on the same team
	other->setDead(true); // setting other's piece as dead
	this->getPoint() = other->getPoint(); // getting current point to other piece's point
}

/*
* the move function
* taking care of case when a king move from one place to another
* gets the dest point, and all of the other pieces in the board
* returns nothing
*/
void King::move(const Point& dest, std::vector<Piece*>& pieces)
{
	bool result = false;
	if (abs(this->getPoint().getX() - dest.getX()) == 1) //if the x distance is 1
	{
		result = true;
	}
	else if (abs(this->getPoint().getY() - dest.getY()) == 1) //or if the y distance is 1
	{
		result = true;
	}
	doesEat(result, dest, getPieceAtPoint(dest ,pieces));
}
