#pragma once

#include <exception>

/*
* exception for a case where an unexiting type of piece trying to be used
* one virtual function, returns the exception reason
*/

class NoType : public std::exception
{
public:
	virtual const char* what() const
	{
		return "This piece type dosent exists.";
	}

};