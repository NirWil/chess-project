#include "Board.h"

/*
* input: none
* output: none
* This is the construor of the board. this is the place where
* the Pieces are created and pushed into the pieces vector.
*/
Board::Board()
{
	/*
	Point kngPoint('a', '3');
	Knight* kng = new Knight(TEAMS::BLACK, kngPoint);
	Point kingPoint('b', '8');
	King* king = new King(TEAMS::WHITE, kingPoint);
	Point rookPoint('c', '8');
	Rook* rook = new Rook(TEAMS::WHITE, rookPoint);
	Point pawnPoint('b', '2');
	Pawn* pawn = new Pawn(TEAMS::BLACK, pawnPoint);
	Point bishopPoint('f', '4');
	Bishop* bishop = new Bishop(TEAMS::WHITE, bishopPoint);
	Point queenPoint('h', '6');
	Queen* queen = new Queen(TEAMS::BLACK, queenPoint);
	this->_pieces.push_back(kng);
	this->_pieces.push_back(king);
	this->_pieces.push_back(rook);
	this->_pieces.push_back(pawn);
	this->_pieces.push_back(bishop);
	this->_pieces.push_back(queen);*/
}

Board::~Board()
{
	for (int i = 0; i < this->_pieces.size(); i++)
	{
		delete this->_pieces[i];
	}
}

/*
* input: none
* output: A refrence of the pieces vector of the board
* A getter of the refrence of the pieces
*/
std::vector<Piece*>& Board::getPieces()
{
	return this->_pieces;
}

/*
* input: none
* output: none
* A method that clears the dead pieces every turn, it deletes them
* and then earses them from the vector
*/
void Board::clearDeadPieces()
{
	int i = 0;
	for (i = 0; i < this->_pieces.size(); i++)
	{
		if (this->_pieces[i]->isDead())
		{
			delete this->_pieces[i];
			this->_pieces.erase(this->_pieces.begin() + i);
		}
	}
}

void Board::insertToBoard(const unsigned int x, const unsigned int y, int type)
{
	Piece* piece;
	Point point (x, y);
	int team = 0;

	if (type == '#' || type == '\n' || type == NULL)
		//just making sure type is valid
	{
		return;
	}

	if (isupper(type))
	{
		team = TEAMS::WHITE;
	}
	else if(islower(type))
	{
		team = TEAMS::BLACK;
	}
	type = tolower(type);

	switch (type)
	{
	case TYPE_AS_CHAR::BISHOP:
		piece = new Bishop(team, point);
		break;
	case TYPE_AS_CHAR::KING:
		piece = new King(team, point);
		break;
	case TYPE_AS_CHAR::KNIGHT:
		piece = new Knight(team, point);
		break;
	case TYPE_AS_CHAR::PAWN:
		piece = new Pawn(team, point);
		break;
	case TYPE_AS_CHAR::QUEEN:
		piece = new Queen(team, point);
		break;
	case TYPE_AS_CHAR::ROOK:
		piece = new Rook(team, point);
		break;
	}

	this->_pieces.push_back(piece);
}
