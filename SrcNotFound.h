#pragma once

#include <exception>

/*
* exception for a case where a player trying to move a piece from unexisting point / place
* one virtual function, returns the exception reason
*/

class srcNotFound : public std::exception
{
public:
	virtual const char* what() const
	{
		return "This src point doesnt exists.";
	}

};