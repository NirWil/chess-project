#pragma once

/*
* An enum that give you the type of the piece, by the number
*/

enum TYPE
{
	KING = 1, // 1 = king
	QUEEN, // 2 = queen
	ROOK, // 3 = rook
	BISHOP, // 4 = bishop
	KNIGHT, // 5 = knight
	PAWN, // 6 = pawn
};
