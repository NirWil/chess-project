#pragma once

#include <string>
#include <vector>
#include <iostream>

#include "Piece.h"
#include "Type.h"

class Queen : public Piece
{
public:
	Queen(int team, Point& point);

	
	virtual void eat(Piece* other) override;
	virtual void move(const Point& dest, std::vector<Piece*>& pieces) override;

	bool isRookMove(const Point& dest, std::vector<Piece*>& pieces);
	bool isBishopMove(const Point& dest, std::vector<Piece*>& pieces);
};
