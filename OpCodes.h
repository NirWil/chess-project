#pragma once

/*
* An enum that give you the id of the number
*/
enum OP_CODES
{
	OK = 0,	            //If the move is vallid and everything is ok :).
	CHESS,			    //If the King is Threanend.
	SRC_NOT_FOUND,      //if the source piece is not found
	ALREDY_TAKEN,       //if the dest place is alredy taken
	SELF_CHESS,         //if self chess happend
	ILEGALL_POINT,      //if the point is ilegall point
	ILEGALL_MOVE,       //if the move is ilegall
	SAME_DST_SRC        //if the dest place is the same as the source place
};