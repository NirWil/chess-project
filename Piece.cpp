#include "Piece.h"

/*
* the constructor
* set the team, point and type
* taking care of exceptions cases
*/

Piece::Piece(int team, int type, Point& point)
{
	if (!Piece::isValidTeam(team)) // if the piece's team isn't valid
	{
		throw NoTeam();
	}
	else if (!Piece::isValidType(type)) // if the piece's type isn't valid
	{
		throw NoType();
	}
	else
	{
		// default settings
		this->_dead = false;
		this->_team = team;
		this->_type = type;
		this->_point = point;
	}

}


/*
* a function to check if piece's type is valid
* gets int - type, to check with the enum
* returns bool, is it valid type - true, or is it not - false
*/
bool Piece::isValidType(int type)
{
	bool result = false; // result start as false
	// after checking with all type, result rathers stays the same
	// because no type matches
	// or becoming true after finding valid type
	switch (type)
	{
	case TYPE::BISHOP:
		result = true;
		break;
	case TYPE::KING:
		result = true;
		break;
	case TYPE::KNIGHT:
		result = true;
		break;
	case TYPE::PAWN:
		result = true;
		break;
	case TYPE::ROOK:
		result = true;
		break;
	case TYPE::QUEEN:
		result = true;
		break;
	default:
		result = false;
		break;
	}
	// if the type isn't a valid type, result stays false
	return result;
}


/*
* a function to check if piece's team is valid
* gets int - type, to check with the enum
* returns bool, is it valid team - true, or is it not - false
*/
bool Piece::isValidTeam(int team)
{
	bool result = false; // result start as false
	if (team != TEAMS::BLACK && team != TEAMS::WHITE)
	{
		// if the team that the function gets, isn't matches team white nor team black
		result = false;
	}
	else
	{
		// if the team that the function gets, matches one of the valid teams
		result = true;
	}
	return result;
}

/*
* a function to get point as string
* gets nothing, using classes functions to get the X & Y
* returns string - the point as string
*/
std::string Piece::getPointAsString()
{
	// using 2 functions, first the get X as char, second to get Y
	// returns as one full string
	std::string result = "" + this->_point.getXasChar() + this->_point.getYasChar();
	return result;
}


/*
* the getters
* there is getter for;
* team
* type
* point
*/


int Piece::getTeam() const
{
	return this->_team; // returns team
}

int Piece::getType() const
{
	return this->_type; // retuns type
}

Point& Piece::getPoint()
{
	return this->_point; // returns point
}


/*
* a function to get piece as char
* gets nothing, using this->_type
* returns char - the piece as char
*/ 

char Piece::getPieceAsChar()
{
	char result = 0;
	switch (this->_type)
	{
	// going through all cases in the enum
	case TYPE::BISHOP:
		result = TYPE_AS_CHAR::TYPE::BISHOP;
		break;
	case TYPE::KING:
		result = TYPE_AS_CHAR::TYPE::KING;
		break;
	case TYPE::KNIGHT:
		result = TYPE_AS_CHAR::TYPE::KNIGHT;
		break;
	case TYPE::PAWN:
		result = TYPE_AS_CHAR::TYPE::PAWN;
		break;
	case TYPE::ROOK:
		result = TYPE_AS_CHAR::TYPE::ROOK;
		break;
	case TYPE::QUEEN:
		result = TYPE_AS_CHAR::TYPE::QUEEN;
		break;
	}
	if (this->_team == TEAMS::WHITE)
	{
		// if the team type is white, changing from CAPS-LOCK
		result = toupper(result);
	}
	return result;
}

/*
* a function to get if piece is dead
* returns bool - is the piece dead or not
*/
bool Piece::isDead() const
{
	return this->_dead;
}

/*
* a function to ser piece as dead
* returns nothing
*/
void Piece::setDead(bool dead)
{
	this->_dead = dead;
}

void Piece::doesEat(bool isMoveOK, const Point& point, Piece* other)
{
	if (isMoveOK && other != nullptr) //if move is valid there is something in that place
	{
		//if place is alredy taken you cant move there, you need to check
		//if its in the enemy team, it eats, if its not, you cant move there.
		if (other->getTeam() == this->getTeam())
		{
			throw AlredyTaken();
		}
		else if (other->getType() == TYPE::KING)
		{
			throw ChessException();
		}
		else
		{
			this->eat(other);
		}
	}
	else if (isMoveOK)
	{
		this->getPoint() = point;
	}
	else
	{
		throw InvalidMove();
	}
}

