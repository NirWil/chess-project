#include "Bishop.h"

/*
* the constructor
* set the team, point and type
* type in this case, always going to be BISHOP
*/
Bishop::Bishop(int team, Point& point)
	:Piece(team, TYPE::BISHOP, point)
{

}

/*
* the eat function
* taking care of case when a bishop piece eating another piece
* gets pointer to the other piece (that being eaten)
* returns nothing
*/
void Bishop::eat(Piece* other)
{
	other->setDead(true);
	this->getPoint() = other->getPoint();
	std::cout << this->getPieceAsChar() << " Is eating " << other->getPieceAsChar() << std::endl;
	return;
}


/*
* the move function
* taking care of case when a rook move from one place to another
* gets the dest point, and all of the other pieces in the board
* returns nothing
*/
void Bishop::move(const Point& dest, std::vector<Piece*>& pieces)
{
	Point point(1, 1);
	bool result = true;
	int yPos = this->getPoint().getY();
	if (abs(this->getPoint().getX() - dest.getX()) == abs(this->getPoint().getY() - dest.getY()))
		// if moving in a diagnoal line
	{
		if (dest.getX() - this->getPoint().getX() > 0 && dest.getY() - this->getPoint().getY() > 0)
			//if moving top right
		{
			for (int i = this->getPoint().getX() + 1; i < dest.getX(); i++)
			{
				yPos++;
				point.setX(i);
				point.setY(yPos);
				if (getPieceAtPoint(point, pieces) != nullptr)
				{
					result = false;
					break;
				}
			}
		}

		else if (dest.getX() - this->getPoint().getX() > 0 && dest.getY() - this->getPoint().getY() < 0)
			//if moving bottom right
		{
			for (int i = this->getPoint().getX() + 1; i < dest.getX(); i++)
			{
				yPos--;
				point.setX(i);
				point.setY(yPos);
				if (getPieceAtPoint(point, pieces) != nullptr)
				{
					result = false;
					break;
				}
			}
		}
		
		else if (dest.getX() - this->getPoint().getX() < 0 && dest.getY() - this->getPoint().getY() > 0)
		//moving top left
		{
			
			for (int i = this->getPoint().getX() - 1; i > dest.getX(); i--)
			{
				yPos++;
				point.setX(i);
				point.setY(yPos);
				if (getPieceAtPoint(point, pieces) != nullptr)
				{
					result = false;
					break;
				}
			}
		}

		else if (dest.getX() - this->getPoint().getX() < 0 && dest.getY() - this->getPoint().getY() < 0)
			//if moving bottom left
		{
			for (int i = this->getPoint().getX() - 1; i > dest.getX(); i--)
			{
				yPos--;
				point.setX(i);
				point.setY(yPos);

				if (getPieceAtPoint(point, pieces) != nullptr)
				{
					result = false;
					break;
				}
			}
		}
	}
	else
	{
		result = false;
	}

	doesEat(result, dest, getPieceAtPoint(dest, pieces));
}
