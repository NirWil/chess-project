#include "Knight.h"
#include <iostream>

/*
* the constructor
* set the team, point and type
* type in this case, always going to be KNIGHT
*/
Knight::Knight(int team, Point& point)
	:Piece(team, TYPE::KNIGHT, point)
{
}

/*
* the eat function
* taking care of case when a kinght piece eating another piece
* gets pointer to the other piece (that being eaten)
* returns nothing
*/
void Knight::eat(Piece* other)
{
	other->setDead(true);
	this->getPoint() = other->getPoint();
	std::cout << this->getPieceAsChar() << " Is eating " << other->getPieceAsChar() << std::endl;
	return;
}

/*
* the move function
* taking care of case when a rook move from one place to another
* gets the dest point, and all of the other pieces in the board
* returns nothing
*/
void Knight::move(const Point& dest, std::vector<Piece*>& pieces)
{
	bool result = false;
	if (abs(this->_point.getY() - dest.getY()) == 2)
	{
		if (abs(this->_point.getX() - dest.getX()) == 1)
		{
			result = true;
		}
	}
	else if (abs(this->_point.getX() - dest.getX()) == 2)
	{
		if (abs(this->_point.getY() - dest.getY()) == 1)
		{
			result = true;
		}
	}
	doesEat(result, dest, getPieceAtPoint(dest, pieces));
}
