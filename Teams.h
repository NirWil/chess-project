#pragma once

/*
* An enum that give you the team of the player
*/

enum TEAMS
{
	BLACK = 0, // team black, first player
	WHITE = 1 // team white, second player
};