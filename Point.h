#pragma once

// the min and max values for each point
#define MIN_POINT 1
#define MAX_POINT 8

#include "InvalidPoint.h"
#include <exception>
#include <string>

using std::to_string;

class Point
{
public:
	Point(const unsigned int x, const unsigned int y);  // the constructor
	Point();  // another constructor
	//Point(Point& other);

	// checkers
	static void vallidChecker(int point);
	static int intChecker(int point);

	// getting x & y values
	int getX() const;
	int getY() const;

	// getting x & y as chars
	char getXasChar() const;
	char getYasChar() const;

	// setting the x & y 
	void setX(int x);
	void setY(int y);

private:
	unsigned int _x; // the x value
	unsigned int _y; // the y value
};

// function that isn't belongs in the class
bool operator== (Point lhs, Point rhs);

