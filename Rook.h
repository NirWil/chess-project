#pragma once

#include <string>
#include <vector>
#include <iostream>

#include "Piece.h"
#include "Type.h"

class Rook : public Piece
{
public:
	Rook(int team, Point& point);

	virtual void eat(Piece* other) override;
	virtual void move(const Point& dest, std::vector<Piece*>& pieces) override;

};
