#include "Pawn.h"

/*
* the constructor
* set the team, point and type
* type in this case, always going to be PAWN
*/
Pawn::Pawn(int team, Point& point)
	:Piece(team, TYPE::PAWN, point)
{
	this->isFirstMove = true;
}

/*
* the eat function
* taking care of case when a king piece eating another piece
* gets pointer to the other piece (that being eaten)
* returns nothing
*/
void Pawn::eat(Piece* other)
{
	//before eating, check if you are on the same team
	other->setDead(true); // setting other's piece as dead
	this->getPoint() = other->getPoint(); // getting current point to other piece's point
	std::cout << this->getPieceAsChar() << " is eating " << other->getPieceAsChar() << std::endl;
}

void Pawn::move(const Point& dest, std::vector<Piece*>& pieces)
{
	bool result = false;
	Point point(this->getPoint().getX(), 1);
	if (canPawnEat(dest, pieces)) //if eating is possible
	{
		result = true;
		if (result)
		{
			this->isFirstMove = false;
		}
	}
	else if (this->isFirstMove) //if first move
	{
		result = firstTimeMove(dest, pieces);
		if (result)
		{
			this->isFirstMove = false;
		}
	}
	else //if regular move
	{
		result = regularMove(dest, pieces);
	}
	doesEat(result, dest, getPieceAtPoint(dest, pieces));
}

bool Pawn::canPawnEat(const Point& dest, std::vector<Piece*>& pieces)
{
	bool result = false;
	Point point(this->getPoint().getX(), 1);
	if (getPieceAtPoint(dest, pieces) != nullptr) //if there is a piece there
	{
		if (this->getTeam() == TEAMS::WHITE)
		{
			if (abs(this->getPoint().getX() - dest.getX()) == 1 && // if x movement is 1
				dest.getY() - this->getPoint().getY() == 1) //and moving up
			{
				result = true;
			}
		}
		else if (this->getTeam() == TEAMS::BLACK)
		{
			if (abs(this->getPoint().getX() - dest.getX()) == 1 && //if x movment is 1
				this->getPoint().getY() - dest.getY() == 1) // and moving down
			{
				result = true;
			}
		}
	}
	return result;
}

bool Pawn::firstTimeMove(const Point& dest, std::vector<Piece*>& pieces)
{
	bool result = false;
	Point point(this->getPoint().getX(), 1);
	if (this->getTeam() == TEAMS::WHITE)
	{
		if (this->getPoint().getX() == dest.getX() &&
			dest.getY() - this->getPoint().getY() == 1 || //y movment is 1
			dest.getY() - this->getPoint().getY() == 2) // or 2
		{
			point.setY(dest.getY());
			if (getPieceAtPoint(point, pieces) == nullptr) // if place is empty
			{
				result = true;
			}
		}
	}
	else if (this->getTeam() == TEAMS::BLACK)
	{
		if (this->getPoint().getX() == dest.getX() &&
			this->getPoint().getY() - dest.getY() == 1 || //y movment is 1
			this->getPoint().getY() - dest.getY() == 2) // or 2
		{
			point.setY(dest.getY());
			if (getPieceAtPoint(point, pieces) == nullptr) // if place is empty
			{
				result = true;
			}
		}
	}
	return result;
}

bool Pawn::regularMove(const Point& dest, std::vector<Piece*>& pieces)
{
	bool result = false;
	Point point(this->getPoint().getX(), 1);
	if (this->getTeam() == TEAMS::WHITE)
	{
		if (this->getPoint().getX() == dest.getX() && //if not moving x
			dest.getY() - this->getPoint().getY() == 1) //moving 1 in y
		{
			point.setY(dest.getY());
			if (getPieceAtPoint(point, pieces) == nullptr) //if place is empty
			{
				result = true;
			}
		}
	}
	else if (this->getTeam() == TEAMS::BLACK)
	{
		if (this->getPoint().getX() == dest.getX() && //not moving x
			this->getPoint().getY() - dest.getY() == 1) //moving 1 in y
		{
			point.setY(dest.getY());
			if (getPieceAtPoint(point, pieces) == nullptr) //if place is empty
			{
				result = true;
			}
		}
	}
	return result;
}

