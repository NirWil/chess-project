#pragma once
// normal includes
#include <iostream>
// header files includes
#include "Point.h"
#include "Piece.h"
#include "OpCodes.h"
#include "King.h"
#include "Knight.h"
#include "Point.h"
#include "Teams.h"
#include "Type.h"
#include "Board.h"
#include "InvalidPoint.h"
#include "ChessException.h"
#include "NotYourTurn.h"
#include "InvalidMove.h"
#include "NoTeam.h"
#include "NoType.h"
#include "AlredyTaken.h"
#include "SrcNotFound.h"
#include "Utils.h"

class Game
{
public:
	Game(); // the constructor
	~Game(); // the destructor

	// functions returns std::string
	std::string parseInput(std::string userInput);
	std::string getBoardAsString();
	
	// void functions
	void saveGame();
	void loadDeafultGame();
	void nextTurn(std::string answer);

private:
	Board board; // the board
	int turn; // the int to determent who's turn is that (white/black)
};
