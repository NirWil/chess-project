#pragma once

#include <string>
#include <vector>
#include "Piece.h"
#include "Type.h"

class King : public Piece
{
public:
	King(int team, Point& point);
	//virtual ~Knight();

	virtual void eat(Piece* other) override;
	virtual void move(const Point& dest, std::vector<Piece*>& pieces) override;

};
