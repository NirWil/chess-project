#pragma once
// normal includes
#include <iostream>
#include <vector>
#include <string>
// header includes
#include "Point.h"
#include "Piece.h"
#include "OpCodes.h"
#include "King.h"
#include "Knight.h"
#include "Rook.h"
#include "Pawn.h"
#include "Bishop.h"
#include "Queen.h"
#include "Point.h"
#include "Teams.h"
#include "Type.h"


class Board
{
	public:
		Board(); // the constructor
		~Board(); // the destructor

		std::vector<Piece*>& getPieces();
		void clearDeadPieces();

		void insertToBoard(const unsigned int x, const unsigned int y, int type);

	// fields
	private:
		// the vector that contains all the game pieces
		std::vector<Piece*> _pieces;
};
