#pragma once

/*
* An enum that give you the the char for the type of the piece
*/

namespace TYPE_AS_CHAR // had to be namespace or else gives an error
{
	enum TYPE
	{
		KNIGHT = 'n', // 1
		ROOK = 'r', // 2
		BISHOP = 'b', // 3
		KING = 'k', // 4
		QUEEN = 'q', // 5
		PAWN = 'p' // 6
	};
}