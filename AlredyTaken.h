#pragma once

#include <exception>

/*
* exception for a case where player trying to move piece to a place that's already taken
* one virtual function, returns the exception reason
*/

class AlredyTaken : public std::exception
{
public:
	virtual const char* what() const
	{
		return "This place is alredy taken!";
	}

};