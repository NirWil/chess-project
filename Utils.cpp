#include "Utils.h"

Piece* getPieceAtPoint(const Point& point, std::vector<Piece*>& pieces)
{
	int i = 0;
	Piece* result = nullptr;
	for (i = 0; i < pieces.size(); i++)
	{
		if (pieces[i]->getPoint() == point)
		{
			result = pieces[i];
			break;
		}
	}
	return result;
}

std::string readFileToString(std::string fileName)
{
    std::ifstream inputFile(fileName);
    std::string result = "";
    std::stringstream inputStream;
    if (inputFile.is_open())
    {
        inputStream << inputFile.rdbuf();
        result = inputStream.str();

        inputFile.close();
    }
    else
    {
        std::cerr << "Unable to open the file " << fileName << std::endl;
        _exit(1); // no need for exception - the game cannot start without the board...
        //just close the program...
    }
    return result;

}
