#pragma once
#include <vector>
#include <iostream>
#include <sstream>
#include <fstream>


#include "Piece.h"
#include "Point.h"

class Piece;
Piece* getPieceAtPoint(const Point& point, std::vector<Piece*>& pieces);

std::string readFileToString(std::string fileName);

//bool doesEat(bool canMoveThere, Piece* eater, Piece* eaten)