#pragma once

#include <exception>

/*
* exception for a case where an unexsiting team is trying to be used
* one virtual function, returns the exception reason
*/

class NoTeam : public std::exception
{
public:
	virtual const char* what() const
	{
		return "This team dosent exists.";
	}

};