#pragma once

#include <exception>

/*
* exception for a case where player trying to move piece to invalid / illegal place
* one virtual function, returns the exception reason
*/

class InvalidMove : public std::exception
{
public:
	virtual const char* what() const
	{
		return "This move is not valid!";
	}

};