#include "Game.h"

/*
* input: none
* output: none
* A constructor for the Game object
*/
Game::Game()
{
	this->turn = TEAMS::WHITE;
}

/*
* input: none
* output: none
* A destructor for the Game object
*/
Game::~Game()
{
}

/*
* input: the client side input about the moves
* output: a string of the answer to the client after the server parses it
* The method that is responsibole for the parsing and checking if the moves are legal.
*/
std::string Game::parseInput(std::string userInput)
{
	std::string answer = std::to_string(OP_CODES::OK);
	char srcX = userInput[0];
	char srcY = userInput[1];
	char dstX = userInput[2];
	char dstY = userInput[3];

	try
	{
		std::cout << "src " << srcX << "," << srcY << std::endl;
		std::cout << "dst " << dstX << "," << dstY << std::endl;
		Point src(srcX, srcY);
		Point dst(dstX, dstY);

		Piece* srcPiece = getPieceAtPoint(src, this->board.getPieces());
		
		std::cout << getBoardAsString() << std::endl;
		if (srcPiece == nullptr)
		{
			std::cout << srcX << "," << srcY << " ";
			throw srcNotFound();
		}
		else if (srcPiece->getTeam() != this->turn)
		{
			throw NotYourTurn();
		}
		else
		{
			srcPiece->move(dst, this->board.getPieces());
		}

	}
	catch (InvalidMove& exception)
	{
		std::cout << exception.what() << std::endl;
		answer = std::to_string(OP_CODES::ILEGALL_MOVE);
	}
	catch (InvalidPoint& exception)
	{
		std::cout << exception.what() << std::endl;
		answer = std::to_string(OP_CODES::ILEGALL_POINT);
	}
	catch (srcNotFound& exception)
	{
		std::cout << "Piece NOT found!!!!" << std::endl;
		std::cout << exception.what() << std::endl;
		answer = std::to_string(OP_CODES::SRC_NOT_FOUND);
	}
	catch (AlredyTaken& exception)
	{
		std::cout << exception.what() << std::endl;
		answer = std::to_string(OP_CODES::ALREDY_TAKEN);
	}
	catch (ChessException& exception)
	{
		std::cout << exception.what() << std::endl;
		answer = std::to_string(OP_CODES::CHESS);
	}
	catch (NotYourTurn& exception)
	{
		std::cout << exception.what() << std::endl;
		answer = std::to_string(OP_CODES::SRC_NOT_FOUND);
	}
	catch (...)
	{
		std::cout << "Unkown exception" << std::endl;
		answer = std::to_string(OP_CODES::ILEGALL_POINT);
	}

	this->nextTurn(answer);
	this->board.clearDeadPieces(); //if a piece die, delete it from memory
	return answer;
}

/*
* input: none
* output: a string of the board acordding to the pieces as char enum
* A method that is used to initiallize the client and to let it know what the board
* is at the start.
*/
std::string Game::getBoardAsString()
{
	std::string result = "";
	Piece* thisPiece;
	Point point(1, 1);
	for (int y = 8; y >= 1; y--)
	{
		for (int x = 1; x <= 8; x++)
		{
			point.setX(x);
			point.setY(y);
			thisPiece = getPieceAtPoint(point, this->board.getPieces());
			if (thisPiece == nullptr) //if there is no piece in this location
			{
				result += '#';
			}
			else
			{
				result += thisPiece->getPieceAsChar();
			}
		}
	}
	result += '1';
	return result;
}

void Game::nextTurn(std::string answer)
{
	if (answer == std::to_string(OP_CODES::OK)) 
	{
		std::cout << "Switching turns" << std::endl;
		if (this->turn == TEAMS::BLACK)
		{
			this->turn = TEAMS::WHITE;
		}
		else if (this->turn == TEAMS::WHITE)
		{
			this->turn = TEAMS::BLACK;
		}
	}
}


/*
* TODO
* To Be Countinued
*/
void Game::saveGame()
{
}

void Game::loadDeafultGame()
{
	int x = 1, y = 8;
	Point thisPoint(1, 1);
	std::string boardStr = readFileToString("deafultBoard.txt");

	for (int i = 0; i < boardStr.size(); i++)
	{
		std::cout << boardStr[i] << " ";
		if (boardStr[i] != '#' && boardStr[i] != '\n')
		{
			this->board.insertToBoard(x, y, boardStr[i]);
		}
		else if (boardStr[i] == '\n')
		{
			std::cout << std::endl;
			y--;
			x = 0;
		}
		x++;
	}
}

