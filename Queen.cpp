#include "Queen.h"

/*
* the constructor
* set the team, point and type
* type in this case, always going to be PAWN
*/
Queen::Queen(int team, Point& point)
	:Piece(team, TYPE::QUEEN, point)
{
}


/*
* the eat function
* taking care of case when a rook piece eating another piece
* gets pointer to the other piece (that being eaten)
* returns nothing
*/
void Queen::eat(Piece* other)
{
	other->setDead(true);
	this->getPoint() = other->getPoint();
	std::cout << this->getPieceAsChar() << " Is eating " << other->getPieceAsChar() << std::endl;
}

void Queen::move(const Point& dest, std::vector<Piece*>& pieces)
{
	bool result = false;
	if (isRookMove(dest, pieces) || isBishopMove(dest, pieces))
	{
		result = true;
	}
	doesEat(result, dest, getPieceAtPoint(dest, pieces));
}

bool Queen::isRookMove(const Point& dest, std::vector<Piece*>& pieces)
{
	Point point(1, 1);
	bool result = true;
	if (abs(this->getPoint().getX() - dest.getX()) > 0 //if x distance is greater than 0 and y is 0
		&& abs(this->getPoint().getY() - dest.getY()) == 0)
	{
		point.setY(dest.getY());
		if (this->getPoint().getX() < dest.getX()) //if moving right
		{
			for (int x = this->getPoint().getX() + 1; x < dest.getX(); x++)
				//if he is passing over something
			{
				point.setX(x);
				if (getPieceAtPoint(point, pieces) != nullptr) //if he hits a piece
				{
					result = false;
					break;
				}
			}
		}
		else if (this->getPoint().getX() > dest.getX()) //if moving left
		{
			for (int x = this->getPoint().getX() - 1; x > dest.getX(); x--)
				//if he is passing over something
			{
				point.setX(x);
				if (getPieceAtPoint(point, pieces) != nullptr) //if he hits a piece
				{
					result = false;
				}
			}
		}
	}
	else if (abs(this->getPoint().getX() - dest.getX()) == 0 //if x distance is greater than 0 and y is 0
		&& abs(this->getPoint().getY() - dest.getY()) > 0)
	{
		point.setX(dest.getX());
		if (this->getPoint().getY() < dest.getY()) //if moving up
		{
			for (int y = this->getPoint().getY() + 1; y < dest.getY(); y++)
				//if he is passing over something
			{
				point.setY(y);
				if (getPieceAtPoint(point, pieces) != nullptr) //if he hits a piece
				{
					result = false;
					break;
				}
			}
		}
		else if (this->getPoint().getY() > dest.getY()) //if moving down
		{
			for (int y = this->getPoint().getY() - 1; y > dest.getY(); y--)
				//if he is passing over something
			{
				point.setY(y);
				if (getPieceAtPoint(point, pieces) != nullptr) //if he hits a piece
				{
					result = false;
				}
			}
		}
	}
	else
	{
		result = false;
	}
	return result;
}

bool Queen::isBishopMove(const Point& dest, std::vector<Piece*>& pieces)
{
	Point point(1, 1);
	bool result = true;
	int yPos = this->getPoint().getY();
	if (abs(this->getPoint().getX() - dest.getX()) == abs(this->getPoint().getY() - dest.getY()))
		// if moving in a diagnoal line
	{
		if (dest.getX() - this->getPoint().getX() > 0 && dest.getY() - this->getPoint().getY() > 0)
			//if moving top right
		{
			for (int i = this->getPoint().getX() + 1; i < dest.getX(); i++)
			{
				yPos++;
				point.setX(i);
				point.setY(yPos);
				if (getPieceAtPoint(point, pieces) != nullptr)
				{
					result = false;
					break;
				}
			}
		}

		else if (dest.getX() - this->getPoint().getX() > 0 && dest.getY() - this->getPoint().getY() < 0)
			//if moving bottom right
		{
			for (int i = this->getPoint().getX() + 1; i < dest.getX(); i++)
			{
				yPos--;
				point.setX(i);
				point.setY(yPos);
				if (getPieceAtPoint(point, pieces) != nullptr)
				{
					result = false;
					break;
				}
			}
		}

		else if (dest.getX() - this->getPoint().getX() < 0 && dest.getY() - this->getPoint().getY() > 0)
			//moving top left
		{

			for (int i = this->getPoint().getX() - 1; i > dest.getX(); i--)
			{
				yPos++;
				point.setX(i);
				point.setY(yPos);
				if (getPieceAtPoint(point, pieces) != nullptr)
				{
					result = false;
					break;
				}
			}
		}

		else if (dest.getX() - this->getPoint().getX() < 0 && dest.getY() - this->getPoint().getY() < 0)
			//if moving bottom left
		{
			for (int i = this->getPoint().getX() - 1; i > dest.getX(); i--)
			{
				yPos--;
				point.setX(i);
				point.setY(yPos);

				if (getPieceAtPoint(point, pieces) != nullptr)
				{
					result = false;
					break;
				}
			}
		}
	}
	else
	{
		result = false;
	}
	return result;
}
