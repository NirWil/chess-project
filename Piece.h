#pragma once
// header files includes
#include "Point.h"
#include "Teams.h"
#include "Type.h"
#include "NoTeam.h"
#include "NoType.h"
#include "AlredyTaken.h"
#include "ChessException.h"
#include "TypeAsChar.h"
#include "InvalidMove.h"
#include "Utils.h"
// normal includes
#include <string>
#include <vector>
// space name
using std::vector;

class Piece
{
public:

	Piece(int team, int type, Point& point); // the constructor

	// static functions
	static bool isValidType(int type);
	static bool isValidTeam(int team);

	//virtual ~Piece() = 0;
	std::string getPointAsString();

	// virtual functios
	virtual void move(const Point& dest, std::vector<Piece*>& pieces) = 0;
	virtual void eat(Piece* other) = 0;

	int getTeam() const;
	int getType() const;
	Point& getPoint();

	char getPieceAsChar();
	bool isDead() const;

	void setDead(bool dead);
	void doesEat(bool isMoveOK, const Point& point, Piece* other);


private:
	int _type; // type of the piece (pawn/queen/etc...)

protected:
	Point _point; // current point where the piece at
	int _team; // which team the piece belongs to
	bool _dead; // is the piece dead (has been eaten) or still in use
};
